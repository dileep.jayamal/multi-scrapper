# -*- coding:utf-8 -*-
# (c) 2020 ChiefDX  All Rights Reserved
#  Dileep Jayamal
import json
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
import signal
from selenium.common import exceptions


class YoutubeSelenium:
    def __init__(self, headless=False):
        chrome_options = Options()
        if headless:
            chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920x1080")
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="./webdriver/chromedriver")

    def search(self, search_query, n=100):
        search_query = search_query.replace(" ", "+")
        self.driver.get("https://www.youtube.com/results?search_query={}".format(search_query))
        time.sleep(10)
        links = []
        actions = ActionChains(self.driver)
        videos = self.driver.find_elements_by_id("thumbnail")
        while len(videos) < n:
            for i in range(2):
                try:
                    actions.move_to_element(videos[-2]).perform()
                    break
                except:
                    time.sleep(10) # On not loaded well
            time.sleep(10)
            videos = self.driver.find_elements_by_id("thumbnail")

        for video in videos:
            try:
                if video.tag_name == 'a':
                    link = video.get_attribute('href')
                    if link is not None:
                        links.append(link.split("=")[1].split("&")[0])
                        print(link)
            except Exception as ex:
                pass
        return links

    def video(self, video_id):
        self.driver.get("https://www.youtube.com/watch?v={}".format(video_id))
        self.driver.maximize_window()
        time.sleep(5)

        try:
            # Extract the elements storing the video title and
            # comment section.
            title = self.driver.find_element_by_xpath('//*[@id="container"]/h1/yt-formatted-string').text
            comment_section = self.driver.find_element_by_xpath('//*[@id="comments"]')
        except exceptions.NoSuchElementException:
            # Note: Youtube may have changed their HTML layouts for
            # videos, so raise an error for sanity sake in case the
            # elements provided cannot be found anymore.
            error = "Error: Double check selector OR "
            error += "element may not yet be on the screen at the time of the find operation"
            return None

        # Scroll into view the comment section, then allow some time
        # for everything to be loaded as necessary.
        self.driver.execute_script("arguments[0].scrollIntoView();", comment_section)
        time.sleep(7)

        # Scroll all the way down to the bottom in order to get all the
        # elements loaded (since Youtube dynamically loads them).
        last_height = self.driver.execute_script("return document.documentElement.scrollHeight")

        while True:
            # Scroll down 'til "next load".
            self.driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")

            # Wait to load everything thus far.
            time.sleep(2)

            # Calculate new scroll height and compare with last scroll height.
            new_height = self.driver.execute_script("return document.documentElement.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height

        # One last scroll just in case.
        self.driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")

        try:
            # Extract the elements storing the usernames and comments.
            username_elems = self.driver.find_elements_by_xpath('//*[@id="author-text"]')
            comment_elems = self.driver.find_elements_by_xpath('//*[@id="content-text"]')
        except exceptions.NoSuchElementException:
            error = "Error: Double check selector OR "
            error += "element may not yet be on the screen at the time of the find operation"
            print(error)
            return None

        print("> VIDEO TITLE: " + title + "\n")
        data = {"title":title, "comments": []}
        for username, comment in zip(username_elems, comment_elems):
            data["comments"].append([username.text, comment.text])
        return data

    def close(self):
        self.driver.service.process.send_signal(signal.SIGTERM)  # kill the specific selenium child proc
        self.driver.quit()



