# -*- coding:utf-8 -*-
# (c) 2020 Azbil Corporation All Rights Reserved
#  Jayamal
import re
from utils import simple_get, is_good_response, log_error
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import re
import json
import time


class IMDB:

    def __init__(self):
        self.creditors = {}
        self.movie = None

    def scrape_movie(self, movie_link):
        raw_html = simple_get(movie_link)
        html = BeautifulSoup(raw_html, 'html.parser')
        credit_summary_items = html.find_all("div", attrs={"class": "credit_summary_item"})
        creditors = {}
        movie = {}
        for each in credit_summary_items:
            creditors[each.find("h4").text.lower()[:-1].replace("rs", "r")] = [elm["href"] for elm in each.find_all("a",
                                                                                                                    href=re.compile(
                                                                                                            r'/name/*'))]
        subtext = html.find("div", attrs={"class": "subtext"})
        duration = subtext.find("time").text.replace("  ", "").replace("\n", "")
        genres = [x["href"].split("=")[1].split("&")[0] for x in subtext.find_all("a") if "genres" in x["href"]]
        release_date = subtext.find("a", attrs={"title":"See more release dates"}).text
        name = html.find("h1").text
        ratings = {"avg": html.find("span", attrs={"itemprop": "ratingValue"}).text,
                   "count": html.find("span", attrs={"itemprop": "ratingCount"}).text,
                   "reviews": html.find("a", attrs={"href":re.compile("/title/.+/ratings.+")})["href"]}
        summary_text = html.find("div", attrs={"class":"summary_text"}).text.replace("  ", "").replace("\n", "")
        cast_div = html.find("div", attrs={"id":"titleCast"})
        notable_credits = []
        notable_credits_divs = cast_div.find_all("tr", attrs={"class": re.compile("(odd|even)")})
        for notable_credit_elm in notable_credits_divs:
            try:
                notable_credit = dict()
                notable_credit["actor"] = notable_credit_elm.find("a", attrs={"href":re.compile("/name/.*")})["href"]
                notable_credit["character"] = notable_credit_elm.find("a", attrs={"href":re.compile("/title/.*")})["href"]
                notable_credits.append(notable_credit)
            except:
                pass
        more_casts = None
        try:
            more_casts = cast_div.find("a", attrs={"href": re.compile("fullcredits.+")})["href"]
        except:
            pass
        casts = {"notable_credits":notable_credits, "more_casts":more_casts}
        storyline_div = html.find("div", attrs={"id":"titleStoryLine"})
        story_line_text = storyline_div.find("div",  attrs={"class":"inline canwrap"}).find("span").text
        plot_keywords = {"key_words": [x.text for x in
                                       storyline_div.find_all("a", attrs={"href": re.compile("/search/keyword.+")})],
                         "more": storyline_div.find("a", attrs={"href": re.compile("/title/.+/keywords.+")})["href"]
                         }
        try:
            mpaa_reviews = storyline_div.find("a", attrs={"href": re.compile("/mpaa.*")}).parent.findNext("span").text
        except:
            mpaa_reviews = ""

        title_details_elm = html.find("div", attrs={"class":"article", "id":"titleDetails"}).text.split("\n")
        key_s = True
        meta= {}
        next_key = None
        for each in title_details_elm:
            if key_s and len(each) > 3 and each[-1] == ":":
                next_key = each.lower()[:-1]
                key_s = False
            elif not key_s and len(each) > 3:
                meta[next_key] = each
                key_s=True
                next_key=None
            elif ":" in each:
                vals = each.split(":")
                meta[vals[0].lower()]=vals[1]
        meta["duration"] = duration
        meta["genres"] = genres
        meta["release_date"] = release_date
        meta["name"] = name
        meta["summary_text"]= summary_text

        movie["meta"] = meta
        movie["creditors"]=creditors
        movie["ratings"] = ratings
        movie["casts"] = casts
        movie["story_line_text"] = story_line_text
        movie["plot_keywords"] = plot_keywords
        movie["mpaa_reviews"] = mpaa_reviews
        self.movie=movie
        return  movie

    def extract_creditors(self, movie=None):
        if not movie and self.movie:
            movie = self.movie
        elif not movie:
            raise Exception("No movie has been selected!")
        creditors = []
        for creditor_key in movie["creditors"].keys():
            creditors += movie["creditors"][creditor_key]





imd = IMDB()
imd.extract_creditors()
imd.extract_creditors({"A":1})
mv = imd.scrape_movie("https://www.imdb.com/title/tt0796366/?ref_=fn_al_tt_2")
imd.extract_creditors()
imd.extract_creditors({"A":1})
imd.extract_creditors()

