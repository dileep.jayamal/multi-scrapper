# -*- coding:utf-8 -*-
# (c) 2020 ChiefDX  All Rights Reserved
#  Dileep Jayamal
import json
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
import signal
import humanfriendly


class Facebook:
    def __init__(self, headless=False):
        chrome_options = Options()
        if headless:
            chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920x1080")
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="./webdriver/chromedriver")

    def get_posts(self, link, include_comments=False, n=3):
        self.driver.get(link)
        self.driver.maximize_window()
        time.sleep(10)

        def rotine(self):
            try:
                self.driver.find_element_by_class_name("autofocus").click()
            except:
                pass
            time.sleep(1)
            try:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            except:
                pass
            time.sleep(2)
            try:
                self.driver.find_element_by_id("expanding_cta_close_button").click()
            except:
                pass

        while n>0:
            rotine(self)
            n = n-1
        rotine(self)
        posts = []
        post_elms = self.driver.find_elements_by_class_name("userContentWrapper")
        for post in post_elms:
            post_dict = {}
            try:
                post_dict["post_message"] = post.find_element_by_xpath('.//div[@data-testid="post_message"]').text
            except:
                pass
            post_dict["num_of_comments"] = 0
            post_dict["num_of_shares"] = 0
            post_dict["num_of_reactions"] = 0
            post_dict["time_posted"] = post.find_element_by_xpath('.//abbr').get_attribute("title")
            try:
                comment_section = post.find_element_by_xpath('.//form[@class="commentable_item"]').text
                reactions = comment_section.split("\nLike\nComment")[0].split("\n")
                for each in reactions:
                    if "comments" in each:
                        post_dict["num_of_comments"] = humanfriendly.parse_size(each.split(" ")[0])
                    elif "shares" in each:
                        post_dict["num_of_shares"] = humanfriendly.parse_size(each.split(" ")[0])
                    else:
                        post_dict["num_of_reactions"] = humanfriendly.parse_size(each)
            except:
                pass
            if include_comments:
                try:
                    elms = post.find_elements_by_xpath(".//span[contains(text(), 'more comments')]")
                    if elms:
                        ActionChains(self.driver).move_to_element(elms[0]).click().perform()
                except Exception as ex:
                    print(ex)
                    pass
                time.sleep(5)
                comment_elms = post.find_elements_by_tag_name("li")
                comments = []
                for comment_elm in comment_elms:
                    if len(comment_elm.text) < 10:
                        continue
                    comment = {}
                    try:
                        comment["datetime"] = comment_elm.find_element_by_xpath('.//abbr[@class="livetimestamp"]').get_attribute("data-tooltip-content")
                    except:
                        pass
                    try:
                        comment["reactions"] = comment_elm.find_element_by_xpath('.//a[@aria-label="See who reacted to this"]').text
                    except:
                        pass
                    try:
                        comment["comment"] = comment_elm.find_element_by_xpath(
                            './/div[@aria-label="Comment"]').find_element_by_xpath('.//span[@dir="ltr"]').text
                    except:
                        pass
                    comments.append(comment)
                post_dict["comments"] = comments
            posts.append(post_dict)

        return posts

    def close(self):
        self.driver.service.process.send_signal(signal.SIGTERM)
        self.driver.quit()


fb = Facebook(headless=True)
txt = fb.get_posts('https://www.facebook.com/HouseofCards/', n=3)
for each in txt:
    print(each)
    for comment in each["comments"]:
        print(comment)
print(txt)



